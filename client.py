import os
import subprocess
import yaml


def replace_tilda_to_home_dir(path_str: str):
    path_split = path_str.split("~")
    if len(path_split) == 2:
        return get_home_dir(path_str.split("~")[-1])
    elif len(path_split) == 1:
        return path_str
    else:
        print("Error: path '{}' not valid\nPlease fix it to ~/path or /path pattern")
        exit(1)


def get_home_dir(additional: str = ""):
    return "{}/{}".format(os.getenv("HOME"), additional)


def get_config():
    with open('config.yml', 'r') as file1:
        return yaml.safe_load(file1)


def init_dir(dirs: dict):
    base_dir = replace_tilda_to_home_dir(dirs["base_dir"])
    once_dir = replace_tilda_to_home_dir(dirs["once_dir"])
    regular_dir = replace_tilda_to_home_dir(dirs["regular_dir"])
    if not (os.path.exists(base_dir)):
        print("Crate Base Dir " + base_dir)
        os.mkdir(base_dir)

    if not (os.path.exists(once_dir)):
        print("Crate Once Dir " + once_dir)
        os.mkdir(once_dir)

    if not (os.path.exists(regular_dir)):
        print("Crate Regular Dir " + regular_dir)
        os.mkdir(regular_dir)


def de_init():
    subprocess.call(["umount", " /mnt/install"])


def get_system_group():
    with open("/etc/group", 'r') as temp_a:
        temp_a = temp_a.read().split("\n")
    group_dict = {}
    for group_str in temp_a:
        if len(group_str.split(":")) > 2:
            gr_str = group_str.split(":")
            group_dict[gr_str[2]] = gr_str[0]
    return group_dict


def get_ent_group():
    temp_a = subprocess.check_output(["getent", "group"]).decode('utf-8').split("\n")
    group_dict = {}
    for group_str in temp_a:
        if len(group_str.split(":")) > 2:
            gr_str = group_str.split(":")
            group_dict[gr_str[2]] = gr_str[0]
    return group_dict


def get_groups():
    all_groups_list = dict(get_system_group(), **get_ent_group())
    user_groups_int = os.getgroups()
    user_group_string = list()
    for i in user_groups_int:
        try:
            temp_a = all_groups_list[str(i)]
        except KeyError:
            print("Error: " + str(i) + " not found")
            continue
        user_group_string.append(temp_a)
    return user_group_string


def mount(cred: dict, from_dir: str, to_dir: str, dfs=False):
    """

    :param cred:
    :param from_dir:
    :param to_dir:
    :param dfs: Я не знаю что такое этот ваш dfs и почему без опции "nodfs" выдается ошибка в астре, скорее всего не тол
    ько в ней, но после введения в домен. Так что если вам (вдруг) будет нужен dfs - при вызове ставьте "тру"
    :return:

    """

    from_dir = replace_tilda_to_home_dir(from_dir)
    to_dir = replace_tilda_to_home_dir(to_dir)

    if dfs:
        dfs = ""
    else:
        dfs = ",nodfs"
    command = ["sudo", "/sbin/mount.cifs",
               "{}".format(from_dir),
               "{}".format(to_dir),
               "-o", "rw,username={},password={},domain={}{}"
               .format(cred["username"],
                       cred["password"],
                       cred["domain"],
                       dfs)]
    print(command)
    print("done {}".format(subprocess.check_output(command).decode("utf-8")))


def init_install_dir(yaml_config: dict):
    try:
        os.mkdir(replace_tilda_to_home_dir(yaml_config["server"]["to_dir"]))
    except FileExistsError:
        pass
    mount(yaml_config["cred"], yaml_config["server"]["from_dir"], yaml_config["server"]["to_dir"])


def mount_dirs(yaml_config: dict):
    """
        Файл должен выглядеть примерно так
        [
                {
                        "group_name": "Admin",
                        "mount": [
                                {
                                        "mount_name": "mount_1_name",
                                        "mount_from": "//mount.dir/1",
                                        "mount_to": "/mnt/mount_1"
                                },
                                ...
                        ]
                },
                ...
        ]
        """
    import json
    file1 = ""
    groups = get_groups()
    try:
        file1 = open(replace_tilda_to_home_dir(yaml_config["config"]["mount_config"]), "r")
    except FileNotFoundError:
        print("Не найден файл")
        exit(1)
    file1 = json.loads(file1.read())
    for iter_group in range(len(file1)):
        if file1[iter_group]["group_name"] in groups:
            for i in file1[iter_group]["mount"]:
                print("MountName = {}".format(i["mount_name"]))
                mount(yaml_config["cred"], i["mount_from"], i["mount_to"])


def init(init_config: dict):
    init_dir(init_config["dirs"])
    init_install_dir(init_config)
    mount_dirs(init_config)
    return 0


if __name__ == "__main__":
    config = get_config()
    get_system_group()
    init(config)
    print(get_groups())

else:
    pass
